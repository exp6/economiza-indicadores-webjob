﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;

namespace IndicadoresWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var config = new JobHostConfiguration();

            if (config.IsDevelopment)
            {
                config.UseDevelopmentSettings();
            }

            var eventHubConfig = new EventHubConfiguration();
            eventHubConfig.AddReceiver("telemetry", "Endpoint=sb://devices-telemetry.servicebus.windows.net/;SharedAccessKeyName=telemetry-key;SharedAccessKey=hGJl3lg5JD/+dwgjKjKXIXMEyNlx/8JXauQagnP/kr0=");
            eventHubConfig.AddReceiver("telemetry-messages", "Endpoint=sb://devices-telemetry.servicebus.windows.net/;SharedAccessKeyName=telemetry-msg-key;SharedAccessKey=9jM0b0pQiBN1JFkGiR5IdrlRKl5LTvfn06kEL7P23iM=");

            config.UseEventHub(eventHubConfig);

            var host = new JobHost(config);
            // The following code ensures that the WebJob will be running continuously
            host.RunAndBlock();
        }
    }
}
