﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;
using PubnubApi;
using Newtonsoft.Json;

namespace IndicadoresWebJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.

        static private Pubnub pubnub;

        public static void ProcessQueueMessage([EventHubTrigger("telemetry")] string eventData)
        {
            Console.WriteLine(eventData);
            PNConfiguration config = new PNConfiguration();
            config.SubscribeKey = "sub-c-cda52064-6758-11e7-9bf2-0619f8945a4f";
            config.PublishKey = "pub-c-56a38c54-26de-4c6a-99d2-9cb7b7e3db29";

            pubnub = new Pubnub(config);

            String str = eventData.ToString();

            dynamic telemetryData = JsonConvert.DeserializeObject(eventData);
            string channelID = telemetryData["num_kit"];

            pubnub.Publish()
                    .Channel(channelID)
                    .Message(str)
                    .Async(new PNPublishResultExt((publishResult, publishStatus) =>
                    {
                        // Check whether request successfully completed or not.
                        if (!publishStatus.Error)
                        {
                            Console.WriteLine("Message successfully published to specified channel");
                            // Message successfully published to specified channel.
                        }
                        else
                        {
                            Console.WriteLine("Request processing failed: " + publishStatus.Category.ToString());

                            // Request processing failed.

                            // Handle message publish error. Check 'Category' property to find out possible issue
                            // because of which request did fail.
                        }
                    }));
        }

        public static void ProcessQueueMessageEventGrid([EventHubTrigger("telemetry-messages")] string eventData)
        {
            Console.WriteLine(eventData);
        }
    }
}
